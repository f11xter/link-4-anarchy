# Link 4 Anarchy

**... with upgrades**

The humble game of Connect Four is brought to life with powerups galore.

## Upgrades

| Upgrade   | Effect                         |
| --------- | ------------------------------ |
| bomb      | destroys an area around it     |
| rocket    | destroys a line above it       |
| mole      | destroys a chosen bottom piece |
| airstrike | destroys all top pieces        |
| clot      | blocks a column for 1 turn     |
