/**
 * @typedef {Object} State
 * @property {"empty" | "red" | "yellow"} colour
 */

const MoveType = {
  Normal: "normal",
  Bomb: "bomb",
  Rocket: "rocket",
  Mole: "mole",
  Airstrike: "airstrike",
  Clot: "clot",
};

let winner = "";
let canMove = true;

/** @type {"red" | "yellow"} */
let turn = "red";

/** @type {State[][]} */
const board = Array.from({ length: 6 }, () =>
  Array.from({ length: 7 }, () => ({ colour: "empty" })),
);

/** @type {{col: number; turns: number}[]} */
let clots = [];

const turnEl = getElementById("turn");
const moveTypeEl = getElementById("move-type");
const moveTypeNormalEl = getElementById("move-type-normal");
const gridEl = getElementById("grid");

if (!(moveTypeEl instanceof HTMLFormElement)) {
  throw new Error("Failed to find move-type form");
}

if (!(moveTypeNormalEl instanceof HTMLInputElement)) {
  throw new Error("Failed to find move-type-normal input");
}

/** @type {HTMLButtonElement[][]} */
const buttonEls = Array.from({ length: 6 }, () => []);

gridEl.querySelectorAll("button").forEach((el, i) => {
  const row = Math.floor(i / 7);
  const col = i % 7;

  buttonEls[row].push(el);

  el.addEventListener("click", () => {
    move(col);
  });
});

/** @param {number} col */
const move = async (col) => {
  if (winner || !canMove) {
    return;
  }

  const row = board.findLastIndex((row) => row[col].colour === "empty");
  const moveType =
    new FormData(moveTypeEl).get("move-type")?.toString() ?? MoveType.Normal;

  if (
    row === -1 &&
    ![MoveType.Rocket, MoveType.Mole, MoveType.Airstrike].includes(moveType)
  ) {
    return;
  }

  canMove = false;

  switch (moveType) {
    case MoveType.Bomb:
      await bombMove(row, col);
      break;
    case MoveType.Rocket:
      rocketMove(col);
      break;
    case MoveType.Mole:
      await moleMove(col);
      break;
    case MoveType.Airstrike:
      airstrikeMove();
      break;
    case MoveType.Clot:
      normalMove(row, col);
      clots.push({ col, turns: 2 });
      break;
    default:
      normalMove(row, col);
  }

  turn = turn === "red" ? "yellow" : "red";
  turnEl.innerText = `${turn}'s turn`;
  turnEl.setAttribute("data-color", turn);
  moveTypeNormalEl.checked = true;

  checkWin();
  setClots();

  canMove = true;
};

/**
 * Place piece, affected by gravity
 * @param {number} row
 * @param {number} col
 */
function normalMove(row, col) {
  board[row][col].colour = turn;
  buttonEls[row][col].setAttribute("data-color", turn);
}

/**
 * Empty in cross shape, affected by gravity
 * @param {number} row
 * @param {number} col
 */
async function bombMove(row, col) {
  [
    [row, col - 1],
    [row, col + 1],
    [row + 1, col],
  ].forEach(([row, col]) => {
    if (board[row]?.[col]) {
      board[row][col].colour = "empty";
      buttonEls[row][col].setAttribute("data-color", "");
    }
  });

  await settleBoard();
}

/**
 * Empty column
 * @param {number} col
 */
function rocketMove(col) {
  board.forEach((row) => (row[col].colour = "empty"));
  buttonEls.forEach((row) => row[col].setAttribute("data-color", ""));
}

/**
 * Empty bottom piece in column
 * @param {number} col
 */
async function moleMove(col) {
  board[5][col].colour = "empty";
  buttonEls[5][col].setAttribute("data-color", "");
  await settleBoard();
}

/** Empty topmost piece of each column */
function airstrikeMove() {
  for (let col = 0; col < 7; col++) {
    const row = board.findIndex((row) => row[col].colour !== "empty");

    if (row === -1) {
      continue;
    }

    board[row][col].colour = "empty";
    buttonEls[row][col].setAttribute("data-color", "");
  }
}

/** Apply gravity to board */
async function settleBoard() {
  for (let row = 4; row >= 0; row--) {
    await new Promise((resolve) => setTimeout(resolve, 100));

    for (let col = 0; col < 7; col++) {
      if (
        board[row][col].colour !== "empty" &&
        board[row + 1][col].colour === "empty"
      ) {
        const colour = board[row][col].colour;
        board[row][col].colour = "empty";
        buttonEls[row][col].setAttribute("data-color", "");
        board[row + 1][col].colour = colour;
        buttonEls[row + 1][col].setAttribute("data-color", colour);
      }
    }
  }
}

function setClots() {
  clots = clots
    .map((x) => ({ col: x.col, turns: x.turns - 1 }))
    .filter((x) => x.turns > 0);

  for (let col = 0; col < 7; col++) {
    buttonEls.forEach((row) => {
      row[col].disabled = clots.map((x) => x.col).includes(col);
    });
  }
}

function checkWin() {
  for (let row = 0; row < 6; row++) {
    for (let col = 0; col < 7; col++) {
      if (board[row][col].colour === "empty") {
        continue;
      }

      /** @type {(State | undefined)[][]} */
      const sets = [
        [board[row][col + 1], board[row][col + 2], board[row][col + 3]],
        [board[row + 1]?.[col], board[row + 2]?.[col], board[row + 3]?.[col]],
        [
          board[row + 1]?.[col + 1],
          board[row + 2]?.[col + 2],
          board[row + 3]?.[col + 3],
        ],
        [
          board[row + 1]?.[col - 1],
          board[row + 2]?.[col - 2],
          board[row + 3]?.[col - 3],
        ],
      ];

      if (
        sets.some((set) =>
          set.every((x) => x && x.colour === board[row][col].colour),
        )
      ) {
        winner = board[row][col].colour;
        turnEl.innerHTML = `${winner} wins <button type="button">Reset</button>`;
        turnEl.setAttribute("data-color", winner);
        turnEl
          .querySelector("button")
          ?.addEventListener("click", () => reset());
        return;
      }
    }
  }

  if (board.every((row) => row.every((col) => col.colour !== "empty"))) {
    winner = "draw";
    turnEl.innerHTML = `Draw <button type="button">Reset</button>`;
    turnEl.setAttribute("data-color", "");
    turnEl.querySelector("button")?.addEventListener("click", () => reset());
  }
}

const reset = () => {
  board.forEach((row) => row.forEach((col) => (col.colour = "empty")));
  buttonEls.forEach((row) =>
    row.forEach((col) => col.setAttribute("data-color", "")),
  );

  clots = [];
  setClots();

  turn = winner === "red" ? "yellow" : "red";
  turnEl.innerText = `${turn}'s turn`;
  turnEl.setAttribute("data-color", turn);
  moveTypeNormalEl.checked = true;

  winner = "";
  canMove = true;
};

/**
 * @param {string} id
 * @returns {HTMLElement}
 */
function getElementById(id) {
  const el = document.getElementById(id);

  if (!el) {
    throw new Error(`Failed to find element with id "${id}"`);
  }

  return el;
}
